
#ifndef SDLSPRITE_H
#define SDLSPRITE_H
#include <SDL.h>
#include <string>
#include <iostream>

/**
 * @class classe liée a la gestion des sprites
 */

class SDLSprite{
private :
    SDL_Surface *surface; /// surface : pointeur qui sert a charger l'image de le texture
    SDL_Texture *texture ; ///texture :texture qui sert a faire la texture en fonction de la surface

    // @TODO HVHVGVLHVVHVHJVHLVLHV
public:
    /**
     * @brief constructeur pour initialiser surface et texture
     */
    SDLSprite();

    /**
     * @brief destructeur pour les pointeurs surface et texture
     */
    ~SDLSprite();

    /**
     * @brief une fonction qui permet de charger une image et faire la texture grace a la surface
     * @param path represente le chemin vers l'image
     */
    void loidTexture(SDL_Renderer *renderer,char *path) ;


    /**
     *
     * @param SDL_Renderer le rendu sur le quel le sprite sera dessiné
     * @param x la position x du sprite sur le rendu
     * @param y la position y du sprite sur le rendu
     * @param w la largeur du sprite
     * @param h la hauteur du sprite
     */
    void drawSprite(SDL_Renderer *renderer ,int x ,int y,int w ,int h);

    void static  drawRectangle(SDL_Renderer *renderer ,int x ,int y,int w ,int h);


};


#endif