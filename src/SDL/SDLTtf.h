
#ifndef  SDLTTF_H
#define SDLTTF_H
#include "SDL_ttf.h"
#include "SDL.h"
#include "point.h"

 class SDLTtf{
 public:
    SDL_Surface * _surface ;
     SDL_Texture * _texture ;

     SDLTtf();
     ~SDLTtf();

     static TTF_Font *loadFont(const char *path ,int size);
     void loadFontFromFile(SDL_Renderer *renderer ,TTF_Font *font ,const char * message ,SDL_Color color);
     void draw(SDL_Renderer* renderer ,Point posMessage );

     void drawS(SDL_Renderer *renderer ,TTF_Font *font ,const char * message ,SDL_Color color,Point posMessage);

};


#endif