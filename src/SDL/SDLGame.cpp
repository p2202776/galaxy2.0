
#include "SDLGame.h"
#include "SDL_image.h"
#include "iostream"

#define height 426
#define width 800
Point point(width,height);
auto tempsActuel = std::chrono::high_resolution_clock::now();

void erreur(){

    std::cerr << "Erreur : " << SDL_GetError() << std::endl;
    SDL_Quit() ;
}

 SDLGame::SDLGame():game(),menu(),ttf1() , m(44100,MIX_DEFAULT_FORMAT,2,248)/*initialise le game*/ { // Initialisation de SDL

    //rectScore ={150,10,vie,10};
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
         erreur();

     }


     // Création de la fenêtre
     fenetre = SDL_CreateWindow("Ma Fenetre SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height,
                                SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE);
     if (fenetre == nullptr) {
         erreur();
     }

     //creation du rendu
     renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
     if (renderer == nullptr) {
         SDL_DestroyWindow(fenetre);
         erreur();
     }
     //TODO SDL trouve l*image en comptatnt depuis l'executable

      //la on initialise toutes les donnees membres SDLGame.h
     //sprite.loidTexture(renderer,"../data/sample.bmp");
     spriteEnnemi.loidTexture(renderer,"../data/sample1.bmp");
     //sprite1.loidTexture(renderer,"../data/sample.bmp");
     balleSprite.loidTexture(renderer,"../data/sample.bmp");
     spriteAvion.loidTexture(renderer,"../data/sample.bmp");
     spriteVague.loidTexture(renderer,"../data/ennemi.bmp");
     balleVague.loidTexture(renderer,"../data/sample.bmp");
     balleAvion.loidTexture(renderer,"../data/sample.bmp");

     msc=m.LoadMusicFromFile("../data/audio/music1.mp3");//pour charger la musique

     font =ttf.loadFont("../data/ttf/police1.ttf",15);
    // font =ttf.loadFont("../data/ttf/aldhabi.ttf",25);

     //ttf.loadFontFromFile(renderer,font,"SCORE :",{255,0,255});
     //ttf.loadFontFromFile(renderer,font,"0 :",{255,0,255});

      buf[30];
      score =0 ;

 }

SDLGame::~SDLGame()=default ;

void SDLGame::sdlDraw() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    initVagueTexture();
    initBalleVagueTexture();
    spriteAvion.drawSprite(renderer,game.avion.getPosX(),game.avion.getPosY(),10,15  );
    initBalleAvionTexture();

    //mes textures
    itoa(score,buf,10) ;
    //ttf.draw(renderer ,{5,5});
    ttf1.drawS(renderer,font,"SCORE :",{255,0,255},{5,5});
    ttf1.drawS(renderer, font, buf, {255, 0, 255}, {60, 5});
    ttf1.drawS(renderer, font, "VIE :", {255, 0, 255}, {100, 5});
    score++;

    if(game.avion.getVie()>=0){
       // game.avion.setVie(game.avion.getVie()-1);
        rectScore ={150,10,game.avion.getVie(),10};
        dessinerScore(rectScore,{0,255,0,0});
    }
    menu.drawMenu(renderer ,font,{255, 0, 255});
}

void SDLGame::update() {

    game.update(game.controlleur);
}

void SDLGame::initTabEnnemiTexture(Ennemi tabEnnemi[]) {
    for(int i=0 ;i<4;i++){
        spriteEnnemi.drawSprite(renderer,tabEnnemi[i].pos.posX,tabEnnemi[i].pos.posY,ennemiWidth,ennemiHeigh);
    }
}

void SDLGame::initVagueTexture() {

    for(int i=0 ;i<game.tabVague.size();i++){
        spriteVague.drawSprite(renderer,game.tabVague[i].pos.posX,game.tabVague[i].pos.posY,ennemiWidth,ennemiHeigh);
    }
}

void SDLGame::initBalleVagueTexture()  {
    for (int i = 0; i < game.tabVague.size(); i++) {
        for (int j = 0; j < game.tabVague[i].tabBalle.size(); j++) {
            balleVague.drawSprite(renderer,game.tabVague[i].tabBalle[j].posBalle.posX,game.tabVague[i].tabBalle[j].posBalle.posY,10,15);
        }
    }
}

void SDLGame::initBalleTexture() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < game.tabEnnemi[i].tabBalle.size(); j++) {
            balleSprite.drawSprite(renderer,game.tabEnnemi[i].tabBalle[j].posBalle.posX,game.tabEnnemi[i].tabBalle[j].posBalle.posY,10,15);
        }
    }
}

void SDLGame::initBalleAvionTexture() {
    for (int i = 0; i <game.avion.getTab().size() ; ++i) {
        balleAvion.drawSprite(renderer,game.avion.getTab()[i].posBalle.posX,game.avion.getTab()[i].posBalle.posY,10,10);

    }
}

void SDLGame::bougerTabEnnemi(Ennemi tabEnnemi[]) {
    for(int i=0 ;i<4;i++){
        tabEnnemi[i].bougerHorizontal();
    }
}

void SDLGame::dessinerScore(Rectangle rect, SDL_Color color) {
    SDL_Rect rectD ={rect.x,rect.y ,200,rect.h};
    //pour les contours du rectangle de score

    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
    SDL_RenderDrawRect(renderer, &rectD);

     //pour remplir le rectangle du score
    SDL_Rect rectVert ={rect.x,rect.y ,rect.w,rect.h};
    SDL_SetRenderDrawColor(renderer, color.r,color.g ,color.b ,color.a );
    SDL_RenderFillRect(renderer, &rectVert);
}

void SDLGame::Renderer(){
   // SDL_RenderClear(renderer);

}

void SDLGame::runProject() {

    // Attendre que l'utilisateur ferme la fenêtre
    bool isOpen = true;
    SDL_Event event;

   m.PlayeMusic(msc);

    while (isOpen) {
        while (SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                case SDL_QUIT:
                    isOpen = false;
                    break;
                    //juste pour la gestion du deplacement de l'avion
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        case SDLK_x:
                            game.controlleur = 'x';
                            break;
                        case SDLK_LEFT:
                            game.controlleur = 'l'; // ou toute autre valeur que vous souhaitez utiliser
                            break;
                        case SDLK_RIGHT:
                            game.controlleur = 'r'; // ou toute autre valeur que vous souhaitez utiliser
                            break;
                        case SDLK_UP:
                            game.controlleur = 'u'; // ou toute autre valeur que vous souhaitez utiliser
                            break;
                        case SDLK_DOWN:
                            game.controlleur = 'd'; // ou toute autre valeur que vous souhaitez utiliser
                            break;
                        case SDLK_q:
                            if (Mix_PausedMusic()) {
                                Mix_ResumeMusic();
                            } else {
                                Mix_PauseMusic();
                            }
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    // Réinitialisez 'controlleur' lorsque la touche est relâchée
                    game.controlleur = '\0';
                    break;

            }
            game.avion.tirer(game.controlleur);
        }

        update();
        sdlDraw();
        SDL_RenderPresent(renderer) ;
        SDL_Delay(16);
        //Renderer();
    }
}
