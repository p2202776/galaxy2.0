#include "menu.h"
#include "SDLSprite.h"
SDLTtf ttf ;
Menu::Menu(){
    posMenu.x = 300;
    posMenu.y = 180;
    posMenu.w = 200;
    posMenu.h = 80;
    for (int i = 0; i < 4; ++i) {
        position[i].posX =posMenu.x +50 ;
        position[i].posY = posMenu.y+((1+i)*20) ;
    }

}



void Menu::drawMenu(SDL_Renderer *renderer, TTF_Font *font, SDL_Color color) {
    SDLSprite::drawRectangle(renderer,posMenu.x,posMenu.y,posMenu.w,posMenu.h);
    ttf.drawS(renderer , font, "Commencer",{255,0,0},position[0]);
    ttf.drawS(renderer , font,  "pause",{255,0,0},position[1]);
}