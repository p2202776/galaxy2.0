#ifndef SDLGAME_H
#define SDLGAME_H

#include "SDL.h"          /**< Inclut le fichier d'en-tête SDL */
#include "SDLSprite.h"    /**< Inclut le fichier d'en-tête SDLSprite */
#include "../core/game.h"/**< Inclut le fichier d'en-tête Game */
#include "SDLMusic.h"     /**< Inclut le fichier d'en-tête SDLMusic */
#include "SDLTtf.h"       /**< Inclut le fichier d'en-tête SDLTtf */
#include "menu.h"
#include <chrono>

/**
 * @brief La classe SDLGame représente le jeu SDL.
 */
class SDLGame {
public:
    SDL_Window* fenetre = nullptr; /**< Fenêtre SDL */
    SDL_Renderer* renderer = nullptr; /**< Renderer SDL */

    int score = 0; /**< Score du jeu */
    Rectangle rectScore; /**< Rectangle pour afficher le score */

    // Gestion des vagues
    float temps();
    float tempsDerniereVague;
    Uint32 time = SDL_GetTicks();
    // Fin de la gestion des vagues

    Game game; /**< Instance du jeu */
    SDLSprite sprite; /**< Sprite SDL */
    SDLSprite spriteEnnemi; /**< Sprite SDL pour les ennemis */
    SDLSprite sprite1; /**< Autre sprite SDL */
    SDLSprite balleSprite; /**< Sprite SDL pour les balles */
    SDLSprite spriteAvion; /**< Sprite SDL pour l'avion */
    SDLSprite spriteVague; /**< Sprite SDL pour la vague */
    SDLSprite balleVague; /**< Sprite SDL pour les balles de la vague */
    SDLSprite balleAvion; /**< Sprite SDL pour les balles de l'avion */

    // Son
    SDLSound m; /**< Son SDL */
    Mix_Music* msc; /**< Musique SDL */

    // Textures
    TTF_Font* font; /**< Police de caractères SDL */
    SDLTtf ttf; /**< Instance de SDLTtf */
    SDLTtf ttf1; /**< Autre instance de SDLTtf */
    char buf[150]; /**< Tampon pour le texte */

    //
    Menu menu ;
    //

    /**
     * @brief Constructeur de la classe SDLGame.
     */
    SDLGame();

    /**
     * @brief Destructeur de la classe SDLGame.
     */
    ~SDLGame();

    /**
     * @brief Méthode pour dessiner les éléments du jeu SDL.
     */
    void sdlDraw();

    /**
     * @brief Méthode pour mettre à jour le renderer SDL.
     */
    void Renderer();

    /**
     * @brief Méthode pour initialiser les textures des ennemis.
     * @param tab Le tableau d'ennemis.
     */
    void initTabEnnemiTexture(Ennemi tab[]);

    // Gestion des vagues
    /**
     * @brief Méthode pour initialiser les textures des ennemis de la vague.

     */
    void initVagueTexture();
    /**
     * @brief Méthode pour initialiser la texture des balles de la vague.

     */
    void initBalleVagueTexture();


    // Fin de la gestion des vagues

    /**
     * @brief Méthode pour déplacer les ennemis.
     * @param tab Le tableau d'ennemis.
     */
    void bougerTabEnnemi(Ennemi tab[]);

    /**
     * @brief Méthode pour initialiser les textures des balles.
     */
    void initBalleTexture();

    /**
     * @brief Méthode pour initialiser les textures des balles de l'avion.
     */
    void initBalleAvionTexture();

    /**
     * @brief Méthode pour dessiner le score.
     * @param rect Le rectangle où afficher le score.
     * @param color La couleur du score.
     */
    void dessinerScore(Rectangle rect, SDL_Color color);

    /**
     * @brief Méthode pour mettre à jour le jeu SDL.
     */
    void update();

    /**
     * @brief Méthode pour exécuter le projet SDL.
     */
    void runProject();
};

/**
 * @brief Méthode pour obtenir le temps actuel.
 * @return Le temps actuel.
 */
inline float SDLGame::temps() {
   return (float)SDL_GetTicks();
}

#endif