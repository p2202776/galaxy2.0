


#ifndef SDLMUSIC_H
#define SDLMUSIC_H
#include <SDL.h>
#include <string>
#include <iostream>
#include <SDL_mixer.h>

class SDLSound {
public:
    SDLSound(int frequency, Uint16 format, int channels, int chunksize);
    ~SDLSound();
    Mix_Chunk* LoadChunkFromFile(const char * pathChunk);
    Mix_Music* LoadMusicFromFile(const char * pathMusic);
    void PlayeChunk(Mix_Chunk* chunk);
    void PlayeMusic(Mix_Music* music);
};


#endif