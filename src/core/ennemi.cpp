#include "ennemi.h"
#include <iostream>


Ennemi::Ennemi():vie(20) ,tempsDernierTir(0.0){
    pos.posX= 250;
    pos.posY=300;
    vitesse=1 ;
    vitesseBalle=5;
}

Ennemi::Ennemi(int _x ,int _y ,int _vie ,float _tempsDernierTir ,int _vitesse,int _vitesseBalle){
    vie= _vie ;
    tempsDernierTir=_tempsDernierTir;
    pos.posX= _x;
    pos.posY=_y;
    vitesse=_vitesse;
    vitesseBalle=_vitesseBalle;
}

void Ennemi::bougerVerticale() {
    this->pos.posY-=vitesse ;
}

void Ennemi::bougerHorizontal() {
    this->pos.posX-=vitesse ;
}


void Ennemi::tirerBalle(float temps) {
   //  // Obtient le temps actuel en millisecondes

    // Vérifie si suffisamment de temps s'est écoulé depuis le dernier tir (par exemple, 1000 millisecondes)
    if (temps- tempsDernierTir >=1000){
        creerBalle();
        tempsDernierTir = temps; // Met à jour le temps du dernier tir
    }
}

void Ennemi::creerBalle() {
    // Utiliser la position stockée pour initialiser la balle

Direction dir ;//juste une direction pour la balle mais pas important pour l'ennemi
    Balle balle(pos.posX, pos.posY,dir);
    tabBalle.push_back(balle);

}
void Ennemi::deplacerBalle() {
    for (int i = 0; i < tabBalle.size(); i++) {
        tabBalle[i].posBalle.posX -= vitesseBalle ;

        // Ajustez la position de la balle en fonction du mouvement de l'ennemi
        //tabBalle[i].posBalle.posX -= vitesse * ((SDL_GetTicks() - tempsDernierTir) / 1000.0);
    }
}






