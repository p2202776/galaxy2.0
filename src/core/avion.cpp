#include "avion.h"
#include <cassert>
#include <iostream>


int  Avion::getPosX() const {
    return this->position.posX;
}
int  Avion::getPosY() const{
    return  this->position.posY;
}

std::vector<Balle> Avion::getTab() const {return this->tab;}

int Avion::getVie() {
    return vie ;
}

void Avion::setVie(int _vie) {
    this->vie =_vie;
}

void Avion::setPosX(int x) {
    this->position.posX =x ;
}

void Avion::setPosY(int y) {
    this->position.posY =y;
}


Avion::Avion()
{
    this->position.posX=0;
    this->position.posY=0;
    this->vitesse = 0;
    this->vie = 1;
    this->dir = Direction::HAUT;
}
Avion::Avion(int x, int y, float vitesse, int vie)
{
    this->position.posX = x;
    this->position.posY = y;
    this->vitesse = vitesse;
    this->vie = vie;
    this->dir = Direction::HAUT;
}

Avion::~Avion()
{
    this->vie=0;
    this->position.posX = -10;
    this->position.posY = -10;
}

void Avion::bougerAvionDroite()
{

        this->position.posX+=vitesse;

        this->position.posX = position.posX;
    this->dir = Direction::DROITE;
}

void  Avion::bougerAvionGauche()
{


        this->position.posX-=vitesse;


        this->position.posX = position.posX;
    this->dir = Direction::GAUCHE;
}

void Avion::bougerAvionHaut()
{

        this->position.posY-=vitesse;


    this->dir = Direction::HAUT;
}

void Avion::bougerAvionBas()
{


        this->position.posY+=vitesse;


    this->dir = Direction::BAS;
}

void Avion::tirer(char c) {
    if(c=='x'){
        Balle b = {this->position.posX,this->position.posY,this->dir};
        this->tab.push_back(b);
        // this->tab[tab.size()-1].balleDirection= this->dir;
    }


}



void  Avion::update(char c) {


    for(int i=0; i<tab.size();i++)
    {
        switch (this->tab[i].balleDirection)
        {
            case Direction::HAUT :

                    this->tab[i].posBalle.posY -=5;
                break;
            case Direction::BAS:
                    this->tab[i].posBalle.posY +=5;
                break;
            case Direction::DROITE:
                    this->tab[i].posBalle.posX +=5;
                break;
            case Direction::GAUCHE:
                    this->tab[i].posBalle.posX -=5;
                break;
        }
    }


}

void  Avion::testRegression()
{
    Avion A;
    Point test(50,50);
    assert(A.position.posX == 0);
    assert(A.position.posY == 0);
    A.bougerAvionDroite();
    assert((A.position.posX-A.vitesse)==0);

    A.bougerAvionHaut();
    assert((A.position.posY-A.vitesse) ==0);

    A.bougerAvionGauche();
    assert((A.position.posX-A.vitesse) == 0);

    std::cout << "------------ test avion reussi ------------ \n";

}
