#pragma once
#include <chrono>

class Timer {
private:
    std::chrono::high_resolution_clock::time_point debut;

public:
    Timer() {
        debut = std::chrono::high_resolution_clock::now();
    }

    float tempsEcoule() {
        auto tempsActuel = std::chrono::high_resolution_clock::now();
        auto dureeEcoulee = std::chrono::duration_cast<std::chrono::duration<float>>(tempsActuel - debut);
        return dureeEcoulee.count();
    }

    void reinitialiser() {
        debut = std::chrono::high_resolution_clock::now();
    }
};