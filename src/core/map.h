#ifndef GALAXY_WAR_MAP_H
#define GALAXY_WAR_MAP_H
#include <cassert>
class Map{
public:
    int dimX,dimY;
    enum Typecase{
        MUR = '#',
        LIBRE ='_'
    };
    Typecase tab [642][425];

    Map();
    Typecase getObjet(int x, int y) const;
    unsigned char getObjetChar(int x, int y) const;

};
inline unsigned char Map::getObjetChar(int x, int y) const {
    assert(x>=0 && x<dimX);
    assert(y>=0 && y<dimY);
    return (char) tab[x][y];
}
inline Map:: Typecase Map::getObjet(int x, int y) const {
    assert(x>=0 && x<dimX);
    assert(y>=0 && y<dimY);
    return tab[x][y];
}

#endif //GALAXY_WAR_MAP_H
