#ifndef PROJETCONCEPTION_AVION_H
#define PROJETCONCEPTION_AVION_H
#include <iostream>
#include <vector>
#include "balle.h"
#include"point.h"
#include "direction.h"

//#include "direction.h"

/**
 * @class Avion
 *
 * @brief Classe qui gere un un avion et ses caractéristiques.
 *
*/

class Avion{
private:
    Point position; ///< Un vecteur pour la position de l'avion
    int vitesse; ///< La vitesse de l'avion qui dira de combien de pixels il doit se deplacer
    Direction dir; ///< Une direction de type enumeree pour la balle
    std::vector <Balle> tab; ///< un tableau dynamique de balles
    int vie; ///< un entier qui represente le vie de l'avion

public:
    /**
     * @brief Recupere la cordonnée x du point
     *
     */

    int getPosX() const;


    /**
     * @brief Recupere la cordonnée y du point
     *
     */

    int getPosY() const;


    /**
     * @brief Recupere la taille du tableau de balles
     *
     */
    std::vector<Balle> getTab() const;

    /**
     *
     * @brief modifie la position x de l'avion
     */

    void setPosX(int x);


    /**
     *
     * @brief modifie la position y de l'avion
     */

    void setPosY(int y);
    /**
     *
     * @brief consturcteur de l'avion, initialise pa position et son vecteur vitesse
     */



    int getVie();
    /**
   * @brief Recupere la vie de l'avion
   *
   */

    void setVie(int vie );
    /**
   * @brief modifie la vie de l'avion
   *
   */

    Avion();

    /**
     *  @brief constructeur qui initialise sa position, sa vitesse, et sa vie
     * @param x position en x
     * @param y position en Y
     * @param vitesse la vitesse de l'avion
     * @param vie son nombre de vie
     */
    Avion(int x, int y, float vitesse, int vie);

    /**
     * @brief destructeur de la classe , mets sa postion et sa vitesse à 0
     */
    ~Avion();

    /**
     * @fn Fonction qui fait bouger l'avion vers la gauche
     * @brief Sa position est modifée
     *
     */
    void bougerAvionGauche();
/**
       * @fn bougerAvionDroite, fonction qui fait bouger l'avion vers le haut
       * @brief Sa position est modifée
       *
       */

    void bougerAvionDroite();

    /**
    * @fn bougerAvionHaut, fonction qui fait bouger l'avion vers le bas
    * @brief Sa position est modifée
    *
    */

    void bougerAvionHaut();

    /**
     * @fn bougerAvionBas, fonction qui fait tirer l'avion
     * @brief Elle crée une balle et lance cette balle, elle sera detruite une fois hors fenetre
     *
     */

    void bougerAvionBas();


    /**
     * @fn tirer,  fonction qui cree une balle et la stock dans le tableau dynamique de balles
     * @brief Elle tire une balle dans une direction donnéé
     *
     */

    void tirer(char c);

    /**
     *
     *
     */


    /**
     * @fn Fonction test de regression
     * @brief S'assure que tout fonctionne bien
     */

    static void testRegression();

    void update(char c);
};

#endif //PROJETCONCEPTION_AVION_H

