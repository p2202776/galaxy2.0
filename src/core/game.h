#ifndef GAME_H
#define GAME_H

#include <vector>
#include "ennemi.h"   /**< Inclut le fichier d'en-tête "ennemi.h" */
#include "avion.h"    /**< Inclut le fichier d'en-tête "avion.h" */
#include "map.h"      /**< Inclut le fichier d'en-tête "map.h" */
#include "config.h"   /**< Inclut le fichier d'en-tête "config.h" */
#include "rectangle.h" /**< Inclut le fichier d'en-tête "rectangle.h" */

/**
 * @brief La classe Game représente le jeu lui-même.
 */
class Game {
public:
    Ennemi e; /**< Instance d'un ennemi */
    Avion avion; /**< Instance d'un avion */
    Ennemi tabEnnemi[4]; /**< Tableau d'ennemis */

     /**< Caractère pour tirer les balles de l'avion */
    char controlleur ;
    Timer timer ;
    float tempsDerniereVague =0;

    // Gestion des vagues
   // int numVague; /**< Numéro de la vague actuelle */
    std::vector<Ennemi> tabVague; /**< Vecteur d'ennemis formant une vague */

    /**
     * @brief Constructeur de la classe Game.
     */
    Game();

    // Méthodes pour la gestion des vagues
    void generationVague(); /**< Méthode pour générer une vague d'ennemis */

    // Fin de la gestion des vagues

    void gestionClavier(char c); /**< Méthode pour gérer les entrées clavier */
    bool verifieCollision(Rectangle rect1, Point balle); /**< Méthode pour vérifier la collision entre un rectangle et une balle */
    void collisionAvionBalle(); /**< Méthode pour gérer les collisions entre l'avion et les balles */
    void collisionEnnemiBalle(); /**< Méthode pour gérer les collisions entre les ennemis et les balles */

    void gameOver(); /**< Méthode pour gérer la fin de partie */
   // void initTabEnnemi(); /**< Méthode pour initialiser le tableau d'ennemis */
   // void UpdateEnnemi(); /**< Méthode pour mettre à jour les ennemis */

   void update(char c);
   void mouveVague();
   void lancerVague(float  temps);
};

#endif