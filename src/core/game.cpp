
#include "game.h"
#include "SDL.h"

inline float temps() {
    return (float)SDL_GetTicks();
}

Game::Game() :e(),avion(10,10,5,30) {


    for (int i=0 ;i<4 ;i++){
        tabEnnemi[i].pos.posX =500;
        tabEnnemi[i].pos.posY=(i+1)*80 ;
        //tabEnnemi[i].tabBalle[i].posBalle = {tabEnnemi[i].pos.posX, tabEnnemi[i].pos.posY};

    }
}

bool Game::verifieCollision(Rectangle rect1 ,Point balle) {
    return ((balle.posX>=rect1.x && balle.posX<rect1.x+rect1.w)&&(balle.posY>=rect1.y && balle.posY<rect1.y+rect1.h)) ;
}

void Game::collisionAvionBalle() {
    Rectangle rectAvion = { avion.getPosX(), avion.getPosY(), avionWidth, avionHeigh };
    // Parcourir toutes les balles ennemies
    for (int i = 0; i < tabVague.size(); ++i) {
        for (int j = 0; j < tabVague[i].tabBalle.size(); j++) {
            Point pos= {tabVague[i].tabBalle[j].posBalle.posX,tabVague[i].tabBalle[j].posBalle.posY};
            if (verifieCollision(rectAvion, pos)) {
                SDL_Log("collision yessssss");
                avion.setVie(avion.getVie()- 1);
            }
        }
    }

}

void Game::collisionEnnemiBalle() {

    // Parcourir toutes les balles ennemies
    for (int i = 0; i < tabVague.size(); ++i) {
        Rectangle rectEnnemi = { tabVague[i].pos.posX,tabVague[i].pos.posX, ennemiWidth, ennemiHeigh };
        for (int j = 0; j < avion.getTab().size(); j++) {
            Point pos= {avion.getTab()[i].posBalle.posX,avion.getTab()[i].posBalle.posY};
            if (verifieCollision(rectEnnemi, pos)) {
                SDL_Log("collision vamossss");
                //avion.setVie(avion.getVie()- 1);
            }
        }
    }

}

void Game::gameOver() {
    if(avion.getVie()==0){
        avion.setPosX(-100);
        avion.setPosY(-100);
    }
      ;
}

//gestion de vagues

void Game::generationVague(){
    // Génération aléatoire du nombre d'ennemis dans la vague (entre 2 et 5)
    srand(time(NULL));
    int numEnemies = rand()%5+2; // Pour générer un nombre aléatoire entre 2 et 5

    // Initialisation des ennemis pour cette vague
    for (int i = 0; i < numEnemies; ++i) {
        int y =rand() % 500; //pour generer la position aleatoire sur le y car mon x est fixe
         Ennemi ennemi(400,y,0,0.0,1,5);
         tabVague.push_back(ennemi);
    }
}

void Game::mouveVague() {
    for(int i=0 ;i<tabVague.size();i++){
        tabVague[i].bougerHorizontal();
        tabVague[i].tirerBalle(temps());
        tabVague[i].deplacerBalle();

    }
}
void Game::lancerVague(float temps) {
    //Uint32 temps = SDL_GetTicks();

    if(temps-tempsDerniereVague>=3000  ){
        generationVague();
        tempsDerniereVague=temps ;
    }

}

void Game::update(char c) {
    gestionClavier(c);
    avion.update(c);
    lancerVague(temps());
    mouveVague();
    collisionAvionBalle();//collision balles  ennemi vers avion
    collisionEnnemiBalle(); //collision balles avion vers ennemis
    gameOver();
}





void Game::gestionClavier(char c ) {
    switch (c) {
        case 'l':
            avion.bougerAvionGauche();
            break;
        case 'r':
            avion.bougerAvionDroite();
            break;
        case 'u':
            avion.bougerAvionHaut();
            break;
        case 'd':
            avion.bougerAvionBas();
            break;
    }
}