#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "balle.h"
#include <iostream>
#include <cassert>
#include <vector>
#include <chrono>
#include "timer.h"

/**
 * @class class liee a la gestion des mes ennemis et leurs balles
 */
class Ennemi {
public:
    Point pos;  /// pos de type position represente la position de l'ennemi
    int vie;    /// vie represente la vie de l'ennemi
    float vitesse; /// represente la vitesse de l'ennemi
    std::vector<Balle> tabBalle; /// tabBalle est un tableau dynamique qui stocke les balles de l'ennemi
    int vitesseBalle;           ///represente la vitesse de la balle
    float tempsDernierTir;     /// represente le temps de dernier tir pour pouvoir espacer les balles

    /**
     * @brief represente le constructeur de ma classe
     */
    Ennemi();

    /**
 * @brief represente le constructeur de ma classe
 */
    Ennemi(int x ,int y ,int vie ,float tempsDernierTir ,int vitesse,int vitesseBalle);



    /**
     * @brief cette fonction permettra de faire bouger l'ennemi de façon verticale
     */




    void bougerVerticale();

    /**
     * @brief cette fonction permettra de faire bouger l'ennemi de façon horizontale
     */
    void bougerHorizontal();

    /**
     * @brief cette fonction permettra de creer une balle et de l'ajouter dans le tableau dynamique
     */
    void creerBalle();

    /**
     * @brief fonction qui permet de regulariser l'espacement des balles en font du temps
     * @param temps variable qui permettra de recuperer le temps ecoulé avec SDL_Geticks()
     */
    void tirerBalle(float temps);

    /**
     * @brief fonction qui permet de deplacer les balles
     */
    void deplacerBalle();
};

#endif